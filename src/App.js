import { render } from "@testing-library/react";
import React from "react";

/**
 * If count is 0 display "POOP_TEXT"
 * If count is divisible by 3 display "COUNTER_TEXT"
 * If count is divisible by 5 display "FOO_TEXT"
 * If count is divisible by 3 && 5 display "COUNTER_FOO_TEXT"
 * If count is not divisible by 3, 5 display "POOP_TEXT"
 * The increment button should increase the "count" variable
 * The decrement button should decrease the "count" variable
 * The "count" variable should never go below 0
 */

const COUNTER_TEXT = "Counter";
const FOO_TEXT = "Foo";
const COUNTER_FOO_TEXT = "🙅🏿‍♂️ Counter Foo 🙅🏿‍♂️";
const POOP_TEXT = "💩";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { count: 0 };

    this.handleClickIncrement = this.handleClick.bind(this);
    this.handleClickDecrement = this.handleClick.bind(this);
  }

  handleClickIncrement = () => {
    this.state({ count: this.state.count + 1 });
  };

  handleClickDecrement = () => {
    this.state({ count: this.state.count - 1 });
  };

  render() {
    return (
      <div style={styles.app}>
        <h1>{count}</h1>
        <h3>{text}</h3>
        <div style={styles.buttons}>
          <button onClick={this.handleClickIncrement}>Increment</button>
          <button onClick={this.handleClickDecrement}>Decrement</button>
        </div>
      </div>
    );
  }
}

const styles = {
  app: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
  },
};
export default App;
